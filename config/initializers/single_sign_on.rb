require 'openid'
require 'openid/consumer/discovery'
require 'openid/extensions/sreg'
require 'openid/extensions/pape'
require 'openid/extensions/ax'

require "#{Rails.root}/lib/openid_server_system.rb"
require "#{Rails.root}/lib/active_record_openid_store/association.rb"
require "#{Rails.root}/lib/active_record_openid_store/nonce.rb"
require "#{Rails.root}/lib/active_record_openid_store/openid_ar_store.rb"

#Single Sign On Variable
Sso::Application.config.single_sign_on = {:key => Digest::SHA1.hexdigest("newsinboard")}