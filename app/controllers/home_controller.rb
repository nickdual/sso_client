class HomeController < ApplicationController

  def index

  end

  def single_sign_on
    if (params[:token].blank?)
      sign_out(:user)
      render json: {:status => :ok, :type => "refresh"}
    else
      token = params[:token]
      nonce = params[:nonce].split(/-/)

      data = ActiveSupport::MessageEncryptor.new(Sso::Application.config.single_sign_on[:key]).decrypt_and_verify(token)
      data[:username] = nonce[0]
      data[:updated_at] = Time.at(nonce[1].to_i).utc

      if (user_signed_in?)
        if (current_user.username != data[:username])
          sign_out(:user)
        else
          if (current_user.global_updated_at < data[:updated_at])
            current_user.update_attributes(:global_updated_at => data[:updated_at])
            #Send Get Request to get up-to-date information from server
            render json: {:status => :ok, :type => "update"}
            response = ActiveSupport::JSON.decode(SingleSignOnHelper.get_global_data({:email => data[:email], :toke => data[:token]}))
            if (response.present?)
              current_user.update_attributes(response)
            end
          else
            render json: {:status => :failure}
          end
          return
        end
      end
      @user = User.global_auth({:nickname => data[:username], :email => data[:email], :token => data[:token]})
      if @user.persisted?
        sign_in(:user, @user)
        render json: {:status => :ok, :type => "refresh"}
      else
        render json: {:status => :failure}
      end
    end
  end
end