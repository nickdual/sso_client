module SingleSignOnHelper
  def self.get_global_data(params)
    response = Faraday.post 'http://localhost:3000/auth/get_profile', params
    return response.body
  end

  def self.update_global_data(params)
    Faraday.post 'http://localhost:3000/auth/update_profile', params
  end
end