#= require_self

#= require_tree ./templates
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers

window.Sso =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  initialize: ->

$(document).ready ->
  Sso.initialize()
