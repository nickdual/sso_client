jQuery(document).ready(function($) {
    if (window.attachEvent)
    // this is necessary for IE8 specifically
        window.attachEvent("onmessage", receiveMessage);
    else
        window.addEventListener("message", receiveMessage, false);
    var url = 'http://localhost:3000/auth/global?' + 'hostname=' + encodeURI(location.protocol + '//' + location.host);
    var iframe = '<iframe id="iframe-global-auth" style="display: none" src="' + url + '"></iframe>';
    $('body').append(iframe);
    function receiveMessage(event)
    {
//        if (event.origin !== "http://localhost:3000")
//            return;
        var data;
        if (event.data == '')
            data = {token: '', nonce: 0}
        else
            data = JSON.parse(event.data)
        var refresh = true, global_auth = false, timer = null;
        if (current_user == null && data.token == '')
            return;
        if ((current_user != null && data.token == '') || (current_user == null && data.token != '')) {
            global_auth = true;
            var message = 'Welcome Guy! You\'ve been logged in by Nib Global Network. '
            if (data.token == '')
                message = 'Welcome Guy! You\'ve been logged out by Nib Global Network. '
            $('body').append('<div class="notification" style=""><p class="notification-message" style="">' + message + 'You are being redirected</p><div class="notify-close-info">click here or press esc key to remove the notification bar</div></div>');
            var tag_notification = $('body').children('.notification');
            var redirect_deny = function(e) {
                console.log('1212')
                tag_notification.animate({top: '-75px'}, 500);
                refresh = false;
                if (timer != null)
                    clearTimeout(timer)
            }
            var keypress_deny = function(e) {
                if (e.which == 27) {
                    redirect_deny(e)
                    $(document).unbind('keyup', keypress_deny);
                }
            }

            tag_notification.animate({top: '0px'}, 500);
            tag_notification.children('.notify-close-info').click(redirect_deny)
            $(document).bind('keyup', keypress_deny);
        }
        else {
            var seg = data.nonce.split('-');
            if (current_user.username != seg[0] || current_user.nonce < seg[1]) {
                global_auth = true;
            }
        }
        if (global_auth == true) {
            var start_time = new Date().getTime(), ajax_long;
            var reg = $.ajax({
                type: 'POST',
                url: '/home/single_sign_on',
                async: true,
                dataType: 'json',
                data: {token: data.token, nonce: data.nonce},
                success: function(response) {
                    ajax_long = new Date().getTime() - start_time;
                    if (response.status == 'ok') {
                        if (response.type == 'refresh') {
                            console.log('12')
                            if (refresh == true) {
                                if (typeof(tag_notification) == false)
                                    location.reload()
                                else {
                                    var timer_timeout = 1500 - ajax_long;
                                    if (timer_timeout < 0)
                                        location.reload()
                                    else
                                        timer = setTimeout(function() {
                                            location.reload()
                                        }, timer_timeout)
                                }
                            }
                        }
                        else if (response.type == 'update') {
                            $('body').append('<div class="notification" style=""><p class="notification-message" style="">Your information was update from global auth server, refresh page to take these changes</p></div>');
                            var tag_notification = $('body').children('.notification');
                            tag_notification.animate({top: '0px'}, 500, function() {
                                setTimeout(function(){
                                    tag_notification.animate({top: '-75px'}, 500, function() {
                                        $(this).remove();
                                    })
                                }, 2000);
                            });
                        }
                    }
                },
                error: function(data) {
                    console.log(data)
                }
            })
        }
    }
});