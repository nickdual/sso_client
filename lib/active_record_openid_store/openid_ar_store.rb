require 'openid/store/interface'

# not in OpenID module to avoid namespace conflict
class ActiveRecordStore < OpenID::Store::Interface
  def store_association(server_url, assoc)
    remove_association(server_url, assoc.handle)
    Association.create(:server_url => server_url,
                       :handle     => assoc.handle,
                       :secret     => assoc.secret.force_encoding("UTF-8"),
                       :issued     => assoc.issued,
                       :lifetime   => assoc.lifetime,
                       :assoc_type => assoc.assoc_type)
  end

  def get_association(server_url, handle=nil)
    assocs = if handle.blank?
               Association.where(:server_url => server_url)
             else
               Association.where(:server_url => server_url, :handle => handle)
             end

    assocs.reverse.each do |assoc|
      a = assoc.from_record
      if a.expires_in == 0
        assoc.destroy
      else
        return a
      end
    end if assocs.any?

    return nil
  end

  def remove_association(server_url, handle)
    Association.delete_all(:server_url => server_url, :handle => handle)  > 0
  end

  def use_nonce(server_url, timestamp, salt)
    return false if Nonce.where(:server_url => server_url, :timestamp => timestamp, :salt => salt).first
    return false if (timestamp - Time.now.to_i).abs > OpenID::Nonce.skew
    Nonce.create(:server_url => server_url, :timestamp => timestamp, :salt => salt)
    return true
  end

  def cleanup_nonces
    now = Time.now.to_i
    Nonce.destroy_all({"$where"=> "this.timestamp > " + (now + OpenID::Nonce.skew).to_s + " OR this.timestamp < " + (now - OpenID::Nonce.skew).to_s})
  end

  def cleanup_associations
    Association.destroy_all({"$where"=> "this.issued + this.lifetime > " + Time.now.to_i.to_s})
  end
end
