require 'openid/association'

class Association
  include Mongoid::Document
  store_in collection: 'open_id_associations'

  field :server_url, :type => String
  field :handle, :type => String
  field :secret, :type => String
  field :issued, :type => Integer
  field :lifetime, :type => Integer
  field :assoc_type, :type => String

  attr_accessible :server_url, :handle, :secret, :issued, :lifetime, :assoc_type

  def from_record
    OpenID::Association.new(handle, secret, issued, lifetime, assoc_type)
  end
end

